<?php include template_dir() . "header.php"; ?>
    <script>
        $(document).ready(function () {
            $('.navigation-holder').addClass('not-transparent');
        })
    </script>

    <div class="shop-inner-page" id="shop-content-<?php print CONTENT_ID; ?>">
        <section class="p-t-100 p-b-50 fx-particles">
            <div class="container">
                <div class="heading mobile-view-title">
                    <h1 class="edit" field="title" rel="content"><?php print content_title(); ?></h1>
                </div>
                <div class="row product-holder">
                    <div class="col-12 col-lg-6">
                        <module type="pictures" rel="content" template="skin-6"/>
                    </div>

                    <div class="col-12 col-lg-6 relative product-info-wrapper">
                        <div class="product-info">
                            <div class="product-info-content">
                                <div class="box">
                                    <div class="heading mobile-view-hide">
                                        <h1 class="edit" field="title" rel="content"><?php print content_title(); ?></h1>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-md-7">
                                            <?php $content_data = content_data(CONTENT_ID);
                                            $in_stock = true;
                                            if (isset($content_data['qty']) and $content_data['qty'] != 'nolimit' and intval($content_data['qty']) == 0) {
                                                $in_stock = false;
                                            }
                                            ?>

                                            <?php if (isset($content_data['sku'])): ?>
                                                <p class="labels"><?php _lang("SKU Number", "templates/bamboo") ?>: <span>#<?php print $content_data['sku']; ?></span></p>
                                            <?php endif; ?>

                                            <?php if ($in_stock == true): ?>
                                                <p class="labels"><?php _lang("
Verfügbarkeit", "templates/bamboo") ?>: <span><?php _lang("
Auf Lager", "templates/bamboo") ?></span></p>
                                            <?php else: ?>
                                                <p class="labels"><?php _lang("
Verfügbarkeit", "templates/bamboo") ?>: <span><?php _lang("Ausverkauft", "templates/bamboo") ?></span></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="description">
                                                <div class="edit typography-area" field="content_body" rel="content">
                                                    <h3><?php print _lang('Beschreibung', 'templates/bamboo'); ?></h3>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

                                                    <h3><?php print _lang('Materialien', 'templates/bamboo'); ?></h3>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <module type="shop/cart_add"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="owl-featured m-b-80"><?php print _lang('<strong>Verbunden</strong> Produkte:', 'templates/bamboo'); ?></h2>
                            <module type="shop/products" related="true" limit="6" hide_paging="true"/>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
<script>

$("#varianted_option").on("change", function(){
    var dataid = $("#varianted_option option:selected").attr('data-id');
    var selectedVariantImageURL = $("[image-id='"+dataid+"']").attr("href");
    $(".shop-inner-page .elevatezoom .elevatezoom-holder img").attr("href", selectedVariantImageURL);
    $(".shop-inner-page .elevatezoom .elevatezoom-holder img").attr("src", selectedVariantImageURL);
    $(".zoomWindow").css("background-image", "url('"+selectedVariantImageURL+"')");
});

</script>

<?php include template_dir() . "footer.php"; ?>