<div class="edit" field="bamboo_blog_sidebar" rel="inherit">
    <div class="sidebar__widget categorySideBar">
        <h6><?php _lang("Kategorien", "templates/bamboo"); ?></h6>
        <hr>
        <div class="edit" field="blog_content_main_wrapper" rel="content">
            <module type="categories" content-id="<?php print PAGE_ID; ?>"/>
        </div>
    </div>
</div>
