<script>
    $(document).ready(function () {
        $('body .main > main').addClass('page-settings');
    });
</script>


<?php
$show_inner = false;
$show_inner_trigger = false;

if (isset($_GET['group']) and $_GET['group']) {
    $group = $_GET['group'];

    if ($group == 'general') {
        $show_inner = 'shop/payments/currency';
    } elseif ($group == 'coupons') {
        $show_inner = 'shop/coupons/admin';
    } elseif ($group == 'taxes') {
        $show_inner = 'shop/taxes/admin';
    } elseif ($group == 'payments') {
        $show_inner = 'shop/payments/admin';
    } elseif ($group == 'invoices') {
        $show_inner = 'shop/orders/settings/invoice_settings';
    } elseif ($group == 'shipping') {
        $show_inner = 'shop/shipping/admin';
    } elseif ($group == 'mail') {
        $show_inner = 'shop/orders/settings/setup_emails_on_order';
    } elseif ($group == 'other') {
        $show_inner = 'shop/orders/settings/other';
    }  elseif ($group == 'quantity') {
        $show_inner = 'shop/products/quantity_status';
    } elseif ($group == 'tax_rates') {
        $show_inner = 'shop/tax_rates/admin';
    } else {
        $show_inner = 'trigger';
        $show_inner_trigger = $group;
    }
}
?>

<?php event_trigger('mw.admin.shop.settings', $params); ?>

<?php if ($show_inner): ?>
    <?php if ($show_inner != 'trigger'): ?>
        <module type="<?php print $show_inner ?>"/>
    <?php else: ?>
        <?php event_trigger('mw.admin.shop.settings.' . $show_inner_trigger, $params); ?>
    <?php endif; ?>

    <?php return; ?>
<?php endif ?>

<div class="card bg-none style-1 mb-0">
    <div class="card-header px-0">
        <h5><i class="mdi mdi-shopping text-primary mr-3"></i> <strong><?php _e("Shop settings"); ?></strong></h5>
        <div>

        </div>
    </div>

    <div class="card-body pt-3 px-0">
        <div class="card style-1 mb-3">
            <div class="card-body pt-3 px-5">
                <div class="row select-settings">
                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=general" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/1.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold">Grundeinstellungen</span><br/>
                                <small class="text-muted">Basic store settings</small>
                            </div>
                        </a>
                    </div>

                    <?php event_trigger('mw.admin.shop.settings.menu', $params); ?>

                    <!-- <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=invoices" class="d-flex my-3">
                            <div class="icon-holder"><i class="mdi mdi-cash-register mdi-20px"></i></div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Invoices'); ?></span><br/>
                                <small class="text-muted">Invoice lists and accounting</small>
                            </div>
                        </a>
                    </div> -->

                    <!--                    <div class="col-12 col-sm-6 col-lg-4">-->
                    <!--                        <a href="?group=mail" class="d-flex my-3">-->
                    <!--                            <div class="icon-holder"><i class="mdi mdi-email-edit-outline mdi-20px"></i></div>-->
                    <!--                            <div class="info-holder">-->
                    <!--                                <span class="text-primary font-weight-bold">Auto respond mail</span><br/>-->
                    <!--                                <small class="text-muted">Email and message settings</small>-->
                    <!--                            </div>-->
                    <!--                        </a>-->
                    <!--                    </div>-->

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="<?php print admin_url(); ?>view:shop/action:productUpselling" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/12.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold">Services (Produkt-Upselling)</span><br/>
                                <small class="text-muted">Creating and managing Services (Produkt-Upselling)</small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="<?php print admin_url(); ?>view:shop/action:thank_you_pages" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/3.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Thank You Pages'); ?></span><br/>
                                <small class="text-muted"> Managing Thank You Pages</small>
                            </div>
                        </a>
                    </div>

                    <!-- <div class="col-12 col-sm-6 col-lg-4">
                        <a href="<?php print admin_url(); ?>view:shop/action:round_price" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/8.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold">Einheitliche Preisrundung</span><br/>
                                <small class="text-muted"> Managing Einheitliche Preisrundung</small>
                            </div>
                        </a>
                    </div> -->

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="<?php print admin_url(); ?>view:shop/load_module:shop__offers" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/13.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Angebote (inkl. Countdown)'); ?></span><br/>
                                <small class="text-muted"> Managing Angebote (inkl. Countdown)</small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="https://drm.software/admin/channel-products?channel=10" class="d-flex my-3" target="_blank">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/4.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Preisvergleichsportale'); ?></span><br/>
                                <small class="text-muted"> Managing Preisvergleichsportale</small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="<?php print admin_url(); ?>view:shop/load_module:legals__shipping-info" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/9.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold">Lieferbedingungen</span><br/>
                                <small class="text-muted">Lieferbedingungen settings</small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="<?php print admin_url(); ?>view:shop/action:Itlawfirm" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/14.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('IT Recht Kanzlei'); ?></span><br/>
                                <small class="text-muted">IT Recht Kanzlei settings</small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="<?php print site_url(); ?>checkout" class="d-flex my-3" target="_blank">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/5.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Checkout'); ?></span><br/>
                                <small class="text-muted">Checkout settings</small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=quantity" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/10.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Lagerbestand anzeigen'); ?></span><br/>
                                <small class="text-muted">Du kannst den Lagerbestand im Shop veröffentlichen oder verbergen.</small>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=tax_rates" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/shop-settings/7.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Tax rates'); ?></span><br/>
                                <small class="text-muted">You can set a taxes for different countries</small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=other" class="d-flex my-3">
                            <div class="icon-holder">
                               <img src="<?php print modules_url(); ?>admin-logo/shop-settings/15.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Other settings'); ?></span><br/>
                                <small class="text-muted">Other settings</small>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
