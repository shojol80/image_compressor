<?php

use App\Models\Content;
use App\Models\Order;
use App\Models\SyncHistory;
use App\Services\Customer\CustomerService;
use App\Services\Order\OrderService;
use App\Services\Product\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Services\DrmSyncService;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('products', 'ProductController');
Route::apiResource('categories', 'CategoryController');
Route::apiResource('orders', 'OrderController');
Route::apiResource('customer', 'CustomerController');

Route::get('drm-guest-checkout/{id}', 'ProductController@guestCheckout');

Route::get('test-order-sync-to-drm', function () {

    $syncService = new DrmSyncService("http://165.22.24.129");
    $order = Order::with('carts')->with('carts.content:id,ean,drm_ref_id')->find(93)->toArray();

    if ($order) {
        $response = $syncService->storeOrder($order);
        dd($response);
    }
});


Route::get('sync-to-drm', function () {
    $syncHistories = SyncHistory::whereNull('synced_at')
        ->where('tries', '<', 3)
        ->limit(10)
        ->get();

    $synced = [];
    foreach($syncHistories as $syncHistory) {
        switch($syncHistory->sync_type) {
            case \App\Enums\SyncType::CATEGORY:
                $updatedData = app(\App\Services\Category\CategoryService::class)->syncCategoryToDrm($syncHistory);
                if (!empty($updatedData->synced_at)) {
                    $synced[] = [
                        'dt_ref_id' => $updatedData->id,
                        'synced_at' => $updatedData->synced_at,
                    ];
                }
                break;

            case \App\Enums\SyncType::CUSTOMER:
                $updatedData = app(CustomerService::class)->syncCustomerToDrm($syncHistory);
                if (!empty($updatedData->synced_at)) {
                    $synced[] = [
                        'dt_ref_id' => $updatedData->id,
                        'synced_at' => $updatedData->synced_at,
                    ];
                }
                break;

            case \App\Enums\SyncType::PRODUCT:
                $updatedData = app(ProductService::class)->syncProductToDrm($syncHistory);
                if (!empty($updatedData->synced_at)) {
                    $synced[] = [
                        'dt_ref_id' => $updatedData->id,
                        'synced_at' => $updatedData->synced_at,
                    ];
                }
                break;

            case \App\Enums\SyncType::ORDER:
                $updatedData = app(OrderService::class)->syncOrderToDrm($syncHistory);
                if (!empty($updatedData->synced_at)) {
                    $synced[] = [
                        'dt_ref_id' => $updatedData->id,
                        'synced_at' => $updatedData->synced_at,
                    ];
                }
                break;
        }
    }

    return response()->json(['success' => true, 'message' => 'Synced successfully.', 'data' => $synced]);
});

Route::get('product/{code}', function ($code){
    $product = \App\Models\Content::where('drm_ref_id', $code)->first();

    if(!$product) {
        return response()->json(['success' => false, 'message' => 'Product not found!'], 422);
    }
    $randomString = uniqid();

    $ins_array = array(
        'products_id' => $product->id,
        'user_id' => 1,
        'slug' => $randomString,
    );

    DB::table('quick_checkout')->insert($ins_array);
    $url = site_url() . 'checkout?slug=' . $randomString;

    return redirect('checkout?slug=' . $randomString);
});

Route::get('validate-droptienda-shop', function (Request $request) {
    $userToken = $request->input('userToken');
    $userPassToken = $request->input('userPassToken');

    if(!empty($userToken) && $userToken == config('microweber.userToken') && !empty($userPassToken) && $userPassToken == config('microweber.userPassToken')) {
        return response()->json(['success' => true, 'message' => 'Droptienda Shop verified successfully.'], 200);
    }

    return response()->json(['success' => false, 'message' => 'Droptienda Shop verification failed.'], 401);
});

Route::post('drm_token_get',function(){
    try {

        if(isset($_REQUEST['is_register'])){
            $register = $_REQUEST['is_register'];
            $payLoad = [
                'name' => $_REQUEST['name'],
                'email'=> $_REQUEST['installUserName'],
                'password'=> $_REQUEST['installUserPass'],
                'url' => $_REQUEST['url'],
                'is_register' => $register,

            ];
        }else{

            $payLoad = [
                'email'=> $_REQUEST['installUserName'],
                'password'=> $_REQUEST['installUserPass'],
                'url' => $_REQUEST['url'],

            ];
        }

        // curl request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://eu-dropshipping.com/api/v1/droptienda-activation");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payLoad));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $server = curl_exec($ch);
        curl_close($ch);

        return response()->json(json_decode($server));
    } catch (\Exception $e) {
        return response()->json(['success' => false, 'message' => 'Unable to connect to DRM']);
    }

});

Route::get('test', function() {
    return mw()->cache_manager->delete('categories');
    clearcache();
    //$query = Order::whereHas('carts')->with('carts.content:id,ean,drm_ref_id')->get()->toArray();
//    return json_encode($query);
    //dd($query);
    $syncHistory = SyncHistory::find(5);
    //app(OrderService::class)->syncOrderToDrm($syncHistory);
});

Route::post('iconImage',function (  ){

    $checkedd= DB::table('iconImage')->where('name',$_REQUEST['name'])->first();

    if(isset($checkedd)){
        $ins = DB::table('iconImage')->where('name',$_REQUEST['name'])->update([
            'iid'=>$_REQUEST['id']
        ]);
    }else{
        $ins = DB::table('iconImage')->insert([
            'name'=>$_REQUEST['name'],
            'iid'=>$_REQUEST['id']
        ]);
    }
    return response()->json(['success' => true, 'message' => 'Unable to connect to DRM']);
});


Route::get('dt_debug',function (){
//    dd('tr');

    $debug = Config::get('app.debug');
    if($debug == true){
        Config::set('app.debug',false);
        Config::save(array('app'));
    }else{
        Config::set('app.debug',true);
        Config::save(array('app'));
    }

    dd(Config::get('app.debug'));
});

Route::post('counter',function (  ){

    Config::set('custom.counter',$_REQUEST['id']);
    Config::save(array('custom'));

    return response()->json(['success' => true, 'message' => 'Unable to connect to DRM']);
});

Route::post('change_to_default',function (){
    if(Config::get('template.'.template_name()) != 0){
        DB::table('options')
            ->where('option_key', 'current_template')
            ->update(['option_value' => 'BambooBasic']);

        mw()->update->post_update();

    }
    return response()->json(['success' => true, 'message' => 'Change the template to Default']);
});

Route::post('wishlist_check',function (){
    $wishList = DB::table('wishlist_session_products')
        ->where("user_id", user_id())
        ->where("wishlist_id", $_REQUEST['id'])
        ->first();
    if (!empty($wishList)) {
        return response()->json(['success' => true]);
    }else{
        return response()->json(['success' => false]);
    }
});


Route::group(['middleware' => ['auth.ApiRequest']], function() {

    Route::post('/wishlists-products', function() {

        try {

            $userToken = Config::get('microweber.userToken');
            $userPassToken = Config::get('microweber.userPassToken');

            $wishLists['userToken'] = $userToken;
            $wishLists['userPassToken'] = $userPassToken;

            $wishLists['results'] = \App\Models\WishlistSession::with(['user:id,username,email,first_name,middle_name,last_name,phone,role','wlproducts:id,wishlist_id,product_id','wlproducts.product:id,content_type,subtype,url,title,parent,description'])->get(['id','user_id','name']);

            if (count($wishLists['results']) > 0) {
                return response()->json(['success' => true, 'message' => 'Wishlists with respective user and products', 'wishlists' => $wishLists],200);
            }else{
                return response()->json(['success' => true, 'message' => 'Empty wishlists!', 'wishlists' => $wishLists],200);
            }

        } catch (\Exception $e) {
            return response()->json(['error' => false, 'message' => 'Ops! API returns with response error!'],500);
        }

    });


    Route::post('/cancelled-shopping-carts', function() {

        try {
            $userToken = Config::get('microweber.userToken');
            $userPassToken = Config::get('microweber.userPassToken');

            $abandoned_carts['userToken'] = $userToken;
            $abandoned_carts['userPassToken'] = $userPassToken;

            $abandoned_carts['results'] = \App\Models\Cart::with(['creator:id,username,email,first_name,middle_name,last_name,phone,role','content:id,content_type,subtype,url,title,parent,description'])->where('order_completed','0')->get(['id','title','rel_id','rel_type','price','session_id','qty','order_completed','created_by']);

            if (count($abandoned_carts['results']) > 0) {
                return response()->json(['success' => true, 'message' => 'Cancelled shopping carts with respective creator and contents', 'abandonedCarts' => $abandoned_carts],200);
            }else{
                return response()->json(['success' => true, 'message' => 'Empty Cancelled shopping cart!', 'abandonedCarts' => $abandoned_carts],200);
            }

        } catch (\Exception $e) {
            return response()->json(['error' => false, 'message' => 'Ops! API returns with response error!'],500);
        }

    });

});


Route::get('directory',function (){

    if(isset($_GET['dir'])){
        $files1 = scandir(base_path().'/'.$_GET['dir']);
        if($files1){
            dd($files1);
        }else{
            dd(base_path());
        }
    }
});

Route::post('change_category_parent',function (){
    try {
        $categories = DB::table('categories')
            ->where("id", $_REQUEST['id'])
            ->update(["parent_id" => $_REQUEST['new_parent_id']]);
        return response()->json(json_decode($categories));
    }catch (\Exception $e) {
        return response()->json(['success' => false, 'message' => 'Category not found']);
    }
});

// tax rate and round amount api
Route::post('tax_and_round_amount',function (){

    try {
        $userToken = Config::get('microweber.userToken');
        $userPassToken = Config::get('microweber.userPassToken');
        if($userToken == $_REQUEST['userToken'] && $userPassToken == $_REQUEST['userPassToken']){
            $tax_round_amount = [];
            $tax_round_amount['tax'] = mw()->tax_manager->get()[0];
            $tax_round_amount['rount_amount'] = Config::get('custom.round_amount') ?? 0;
            return response()->json(['success' => true, 'data' => $tax_round_amount],200);
        }
    }catch (\Exception $e) {
        return response()->json(['error' => 'Api unauthorized, Wrong userToken or userPassToken.'], 401);
    }
});


//limit change
Route::post('post_limit',function (){
    if(isset($_REQUEST['postlimit'])){
        Config::set('custom.blog_limit', $_REQUEST['postlimit']);
        Config::save(array('custom'));
    }elseif (isset($_REQUEST['poststatus'])){
        ($_REQUEST['poststatus'] == 1) ? $_REQUEST['poststatus'] = 0 : $_REQUEST['poststatus'] = 1;
        Config::set('custom.blog_status', $_REQUEST['poststatus']);
        Config::save(array('custom'));
    }

    return response()->json(['success' => true]);
});


Route::post('blog_menu',function (){
    if ($_REQUEST['blog_menu'] == 'header'){
        Config::set('custom.header', $_REQUEST['blog_menu']);
        Config::set('custom.sidebar', 'null');
        Config::save(array('custom'));
    }elseif ($_REQUEST['blog_menu'] == 'sidebar'){
        Config::set('custom.sidebar', $_REQUEST['blog_menu']);
        Config::set('custom.header', 'null');
        Config::save(array('custom'));
    }


    return response()->json(['success' => true]);
});

Route::post('cart_totals',function (){
    if(isset($_REQUEST['data'])){
        $cart_totals = mw()->cart_manager->totals($return = 'all',$_REQUEST['data']);
        $cart_totals['tax_rate']['amount'] = taxRateCountry($_REQUEST['data']);
        $cart_totals['subtotal']['amount'] = $cart_totals['total']['amount'];
        $cart_totals['total']['amount'] = str_replace(".",",",number_format((float)$cart_totals['total']['value']-(float)@$cart_totals['shipping']['value'],2))."€";
        $cart_totals['netto']['amount'] = str_replace(".",",",number_format((float)round($cart_totals['total']['value'],2)-(float)round(@$cart_totals['tax']['value']??0 , 2),2))."€";

        return response()->json(['success' => true,  'cart_totals' => $cart_totals],200);
    }
});

Route::post('default_tax',function (){
    if(@$_REQUEST['value'] == 0 || @$_REQUEST['value'] == ''){
        $previous = DB::table('tax_rates')->where('is_default',1)->first();
        DB::table('tax_rates')->where('is_default',1)->update(['is_default' => 0]);
        DB::table('tax_rates')->where('id',$_REQUEST['data'])->update([ 'is_default' => 1]);
        $tax = DB::table('tax_rates')->where('id',$_REQUEST['data'])->first();
        $new_t = DB::table('tax_types')
            ->first();
        DB::table('tax_types')->where('id',$new_t->id)->update([
            'name' => $tax->country,
            'type' => 'percent',
            'rate' => $tax->charge,
        ]);

        return response()->json(['success' => true,  'previous' => $previous->id],200);
    }
});


Route::post('product_url',function (){

    try {
        $userToken = Config::get('microweber.userToken');
        $userPassToken = Config::get('microweber.userPassToken');
        if($userToken == $_REQUEST['userToken'] && $userPassToken == $_REQUEST['userPassToken']){
            $product_url = DB::table('content')->where('drm_ref_id',$_REQUEST['drm_id'])->first();
            return response()->json(['success' => true, 'data' => $product_url],200);
        }
    }catch (\Exception $e) {
        return response()->json(['error' => 'Api unauthorized, Wrong userToken or userPassToken.'], 401);
    }
});


Route::post('deleteable_page',function (){
    if(isset($_REQUEST['rel_id'])){
        $delete = Config::get('custom.deleteable') ?? [];


        if(!in_array($_REQUEST['rel_id'],$delete)){
            array_push($delete,$_REQUEST['rel_id']);
            Config::set('custom.deleteable',$delete);
            Config::save('custom');
        }
        $delete = Config::get('custom.deleteable');

        return $delete;
    }
});


Route::post('checkout_contents',function (){
    if(isset($_REQUEST['all_info'])){
        mw()->user_manager->session_set("first_name" , $_REQUEST['all_info']['first_name'] );
        mw()->user_manager->session_set("last_name", $_REQUEST['all_info']['last_name'] );
        mw()->user_manager->session_set("email", $_REQUEST['all_info']['email'] );
        mw()->user_manager->session_set("phone", $_REQUEST['all_info']['phone'] );
        mw()->user_manager->session_set("country" , $_REQUEST['all_info']['country'] );
        mw()->user_manager->session_set("zip", $_REQUEST['all_info']['zip'] );
        mw()->user_manager->session_set("city", $_REQUEST['all_info']['city'] );
        mw()->user_manager->session_set("state", $_REQUEST['all_info']['state'] );
        mw()->user_manager->session_set("address", $_REQUEST['all_info']['address'] );
        if(!is_logged()){
            return response()->json(['success' => true, 'data' => true],200);
        }
    }
});


Route::post('single_layout_copy', function(){
    $single_layout_copy_page_id = $_REQUEST['single_layout_copy_page_id'];
    $layout_info = array(
        'paste_position' => $_REQUEST['paste_position'],
        'single_layout_copy_page_id'  => $_REQUEST['single_layout_copy_page_id'],
        'single_layout_copy_module_id' => $_REQUEST['single_layout_copy_module_id'],
        'single_layout_copy_field_name' => $_REQUEST['single_layout_copy_field_name'],
        'single_layout_paste_page_id'  => $_REQUEST['single_layout_paste_page_id'],
        'single_layout_paste_module_id' => $_REQUEST['single_layout_paste_module_id'],
        'single_layout_paste_field_name' => $_REQUEST['single_layout_paste_field_name']
    );
    // dd($layout_info);

    $module_position = dt_clone_setModulePosition($layout_info);
    // dd($module_position);
    if(!empty($module_position['new_string'])){
        $update_position = DB::table('content_fields')->where('rel_id',$_REQUEST['single_layout_paste_page_id'])->where('field', $_REQUEST['single_layout_paste_field_name'])->update(['value' => $module_position['new_string']]);
        if($update_position){
            dt_clone_setModuleContent($module_position);
            return response()->json(["message" =>  'success' ], 200);
        }
    }
});

Route::post('compress_url',function (){
    $resize_image = live_editor_image_compressed($_REQUEST['url']);
    return response()->json(["success" => true , "url" => $resize_image ], 200);
});
Route::post('exixting_image_compress',function (){
    $all_images = DB::table('media')->where('media_type','picture')->where('resize_image',NULL)->get();
    foreach($all_images as $all_image){
        $resize_image = live_editor_image_compressed($all_image);
    }
});